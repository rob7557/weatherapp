//
//  ViewController.swift
//  WeatherApp
//
//  Created by Robert Tratseuski on 3/8/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tempretureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
     
    override func viewDidLoad() {
        super.viewDidLoad()
     searchBar.delegate = self
        
    }
}

extension ViewController : UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        
        let urlString = "https://api.apixu.com/v1/current.json?key=5d775f25f45c4d00b82144223190803&q=\(searchBar.text! .replacingOccurrences(of: " ", with: "%20"))"
        
        let url = URL(string:  urlString)
        
        var locationName: String?
        var tempreture: Double?
        var errorHasOccured: Bool = false
        
        let task = URLSession.shared.dataTask(with: url!) {[weak self] (data, response, error) in
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                as! [String : AnyObject]
                
                if let _ = json["error"]{
                    errorHasOccured = true
                }
                
                if let location = json["location"] {
                    locationName = location["name"] as? String
                }
                
                if let current = json["current"] {
                    tempreture = current["temp_c"] as? Double
                }
                
                DispatchQueue.main.async {
                    
                    if errorHasOccured {
                         self?.cityLabel.text = "Error has occured!"
                        self?.tempretureLabel.isHidden = true
                    }
                    else
                    {
                        self?.cityLabel.text = locationName
                        self?.tempretureLabel.text = "\(tempreture!)"
                        
                        self?.tempretureLabel.isHidden = false
                    }
                    
                    
                }
                
            }
            catch let jsonError{
                print(jsonError )
            }
        }
        task.resume()
    }
}
